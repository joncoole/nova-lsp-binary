const path = require('path');
const fssync = require('fs');
const fs = require('fs').promises;
const { parseArgs } = require('node:util');
const { createHash } = require('node:crypto');

const args = parseBuildArgs().values;

(async() => {
	await repoGeneration(args.build_dir, args.repo_dir);
})().catch(console.error)

function parseBuildArgs() {
	return parseArgs({
	  options: {
		build_dir: {
			type: "string",
			short: "b",
		},
		repo_dir: {
			type: "string",
			short: "o",
		},
	  },
	  strict: true,
	});
}

function fileHash(path) {
	const fileBuffer = fssync.readFileSync(path);
	const hashSum = createHash('sha256');
	hashSum.update(fileBuffer);
	
	return hashSum.digest('hex');
};

async function repoGeneration(build_path, repo_path) {
	let repo = {};
	let arch_dirs = await fs.readdir(build_path, { withFileTypes: true });
	
	for (const arch of arch_dirs) {
		if (arch.isDirectory()) {
			let test = await architectureSHAs(arch);
			repo[arch.name] = test;
		}
	}
	
	fs.writeFile(repo_path, JSON.stringify(repo, null, 4), 'utf8');
};

async function architectureSHAs(arch_dir) {
	let arch_path = path.resolve(arch_dir.path, arch_dir.name);
	let binaries = await fs.readdir(arch_path, { withFileTypes: true });
	
	let arch_binaries = {};
	for (const bin of binaries) {
		const bin_path = path.resolve(bin.path, bin.name);
		const hash = fileHash(bin_path);
		arch_binaries[bin.name] = {
			name: bin.name,
			sha256: hash
		}	
	}
	
	return arch_binaries;
};

